#!/bin/sh 
FOLDER=$(zenity --file-selection --directory --title="Selecciona el directorio de destino")
convert -background none "$1" $FOLDER/"`basename "$1" .svg`".png | zenity --progress --title="Convert" --text="Convirtiendo..." --auto-close
exit 0 
