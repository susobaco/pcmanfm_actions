#!/bin/sh
#!/bin/bash
for audiofile in *.{mp3,m4a,mp1,mp2,mp4,mpeg,mpg,webm,wav,acc,ac3,flac}; do
  ffmpeg -i "$1" -ab 192k -ar 44100 -vn -acodec libvorbis -y "${1%.*}.ogg"
done

