#!/bin/sh
#!/bin/bash
for audiofile in *.{mp3,m4a,mp1,mp2,mp4,mpeg,mpg,webm,ogg,acc,ac3,flac}; do
  ffmpeg -i "$1" -ar 44100 -vn -y "${1%.*}.wav"
done

