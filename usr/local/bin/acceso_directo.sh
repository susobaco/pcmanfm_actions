#!/bin/bash
cd "$1"
result="${PWD##*/}"
echo $result
cd ..
for folder in "$1"
do
  ln -s "$folder" "Acceso directo a $result"
done
exit 0