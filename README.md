# pcmanfm_actions

**Quick Action Sripts for pcmanfm file manager.**

¡¡¡¡¡Important update for debian buster and ubuntu bionic!!!!

Installation: 

The file-manager folder must be copied into the .local/share user directory.

The other folder goes in the defined directory /usr/local/bin . Scripts need execution permissions

The content must also be copied into

These scripts are designed to work optimally in Trisquel mini Flidias. To do this, the following packages must be installed:

sudo apt install ffmpeg unar trash-cli blueman zenity thundebird imagemagick


_________________________________________________________________________________________________________________________________________________

**Sripts de acción rápida para el gestor  de archivos pcmanfm**

¡¡¡¡Actualización importante para debian buster y ubuntu bionic!!!!

Instalación: 

Hay que copiar la carpeta file-manager dentro del directorio de usuario .local/share

La otra carpeta va en el directorio definido /usr/local/bin . Los scripts, necesitan permisos de ejecución.

Hay que copiar también el contenido en /usr/share/polkit-1/actions

Estos escripts están diseñados para funcionar de manera óptima en Trisquel mini Flidias. Para ello hay que instalar los siguientes paquetes:

sudo apt install ffmpeg unar trash-cli blueman zenity thunderbird imagemagick
